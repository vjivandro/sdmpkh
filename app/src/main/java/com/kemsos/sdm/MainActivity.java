package com.kemsos.sdm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txt_username;
    String username, idpersonal;
    SharedPreferences sharedPreferences;

    private static final String TAG_USERNAME = "nik";
    private static final String TAG_ID_PERSONAL = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_username = (TextView) findViewById(R.id.txt_username);
        sharedPreferences = getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);

        username = getIntent().getStringExtra(TAG_USERNAME);
        idpersonal = getIntent().getStringExtra(TAG_ID_PERSONAL);

        txt_username.setText(username);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(LoginActivity.session_status, false);
            editor.putString(TAG_USERNAME, null);
            editor.commit();

            Intent inten = new Intent(MainActivity.this, LoginActivity.class);
            finish();
            startActivity(inten);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
