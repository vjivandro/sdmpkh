package com.kemsos.sdm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegristrasiActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Button btn_login, btn_register;
    EditText txt_nik, txt_nama, txt_email, txt_hp, txt_posisi;
    Intent inten;

    boolean success;
    ConnectivityManager conMgr;

    private String url = Server.URL + "pendaftaran";

    private static final String TAG = RegristrasiActivity.class.getSimpleName();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "msg";

    String tag_json_obj = "json_obj_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regristrasi);

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()){

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        btn_login = (Button) findViewById(R.id.btn_login);
        btn_register = (Button) findViewById(R.id.btn_register);
        txt_nik = (EditText) findViewById(R.id.txt_nik);
        txt_nama = (EditText) findViewById(R.id.txt_nama);
        txt_hp = (EditText) findViewById(R.id.txt_hp);
        txt_email = (EditText) findViewById(R.id.txt_email);
        txt_posisi = (EditText) findViewById(R.id.txt_posisi);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inten = new Intent(RegristrasiActivity.this, LoginActivity.class);
                finish();
                startActivity(inten);
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nik = txt_nik.getText().toString();
                String nama = txt_nama.getText().toString();
                String nohp = txt_hp.getText().toString();
                String email = txt_email.getText().toString();
                String posisi = txt_posisi.getText().toString();

                if (conMgr.getActiveNetworkInfo() != null
                        && conMgr.getActiveNetworkInfo().isAvailable()
                        && conMgr.getActiveNetworkInfo().isConnected()){
                    checkRegister(nik, nama, nohp, email, posisi);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void checkRegister(final String nik, final String nama, final String nohp, final String email, final String posisi) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Register ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Register Respone: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getBoolean(TAG_SUCCESS);

                    if (success == true) {
                        Log.e("Successfully Register!", jObj.toString());

                        Toast.makeText(getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                        txt_nik.setText("");
                        txt_nama.setText("");
                        txt_hp.setText("");
                        txt_email.setText("");
                        txt_posisi.setText("");

                    } else {
                        Toast.makeText(getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"Login Eror: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", nama);
                params.put("nik", nik);
                params.put("nohp", nohp);
                params.put("email",email);
                params.put("posisi", posisi);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void showDialog() {
        if(!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onBackPressed(){
        inten = new Intent(RegristrasiActivity.this, LoginActivity.class);
        finish();
        startActivity(inten);
    }

}
